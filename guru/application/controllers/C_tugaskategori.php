<?php

class C_tugaskategori extends CI_Controller{


	public function index()
	{	
		if($this->M_login->logged_id())
		{

		$data['varKategoriTugas']= $this->M_tugaskategori->getTugasKategori();
		$this->load->view('templates/sidebar');
		$this->load->view('tugaskategori/index',$data);
		$this->load->view('templates/footer');

		}else{

			$this->session->set_flashdata('notif','<div class="alert alert-warning">Maaf! Silahkan Login Dahulu</div>');
			//jika session belum terdaftar, maka redirect ke halaman login
			redirect("C_login");
		}
	}

	public function tambah(){
		$this->load->view('templates/sidebar');
		$this->load->view('tugaskategori/tambah');
		$this->load->view('templates/footer');
	}
	function tambah_aksi()
			{ //validasi
		$this->form_validation->set_rules('nama_kategori_tugas', 'nama_kategori_tugas', 'required|min_length[3]|max_length[30]');

		if ( $this->form_validation->run() === FALSE ) {
			$this->load->view('templates/sidebar');
			$this->load->view('tugaskategori/tambah');
			$this->load->view('templates/footer');
		}else{
//input tb
			$data = [
				'nama_kategori_tugas' => $this->input->post('nama_kategori_tugas')
			];
				
			$insert = $this->M_tugaskategori->input_data($data);
			

			if ($insert) {
			$this->session->set_flashdata('notif','<div class="alert alert-warning">Kategori Tugas berhasil ditambah</div>');
				redirect('C_tugaskategori/index');
			} else {
			$this->session->set_flashdata('notif','<div class="alert alert-warning">Terjadi Kesalahan saat regisrasi</div>');
				redirect('C_tugaskategori/index');
			}

		}
	}

	function hapus($id_mapel){
			$where = array('id_mapel' => $id_mapel);
			$this->M_mapel->hapus_data($id_mapel,'dataa');
			redirect('C_mapel/index');
			}
}