<?php

class C_diskusi Extends CI_Controller{

	public function index()
	{
		if($this->M_login->logged_id())
		{

		//$data['varData']= $this->M_user->tampil_data();	//foto
		$data['varDiskusi']= $this->M_diskusi->getDiskusi();
		$this->load->view('templates/sidebar');
		$this->load->view('diskusi/index');
		$this->load->view('templates/footer');		

		}else{

			$this->session->set_flashdata('notif','<div class="alert alert-warning">Maaf! Silahkan Login Dahulu</div>');
			//jika session belum terdaftar, maka redirect ke halaman login
			redirect("C_login");

		}
	}
	public function tambah(){
		$data['varJurusan']=$this->M_jurusan->getJurusan();		//ambil id jurusan
		$data['varKelas']=$this->M_kelas->getKelas();		//ambil id kelass
		$this->load->view('templates/sidebar');
		$this->load->view('diskusi/tambah');
		$this->load->view('templates/footer');
	}


}