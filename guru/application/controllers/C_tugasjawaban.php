<?php

class C_tugasjawaban extends CI_Controller{


	public function index()
	{	
		if($this->M_login->logged_id())
		{

		//$data['varData']= $this->M_user->tampil_data();
	    //$dt['varmateri']= $this->M_materi->getMateriByID($id_materi);
		$this->load->view('templates/sidebar');
		$this->load->view('tugasjawaban/index');
		$this->load->view('templates/footer');

		}else{

			$this->session->set_flashdata('notif','<div class="alert alert-warning">Maaf! Silahkan Login Dahulu</div>');
			//jika session belum terdaftar, maka redirect ke halaman login
			redirect("C_login");
		}
	}
}