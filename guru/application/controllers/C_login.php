<?php
class C_login extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        $this->load->library(array('form_validation'));
    }


	public function index()
	{

		$this->load->view('login/head');
		$this->load->view('login/index');
	}

	 public function proses_login(){
		if($this->M_login->logged_id())
			{
				//jika memang session sudah terdaftar, maka redirect ke halaman dahsboard
				redirect(base_url('C_dashboard')); 

			}else{

				//jika session belum terdaftar

				//set form validation
	            $this->form_validation->set_rules('nik_guru', 'nik_guru', 'required');
	            $this->form_validation->set_rules('password', 'Password', 'required');

	            //cek validasi
				if ($this->form_validation->run() == TRUE) {

				//get data dari FORM
	            $nik_guru = $this->input->post("nik_guru", TRUE);
	            $password = MD5($this->input->post('password', TRUE));
	            
	            //checking data via model
	            $checking = $this->M_login->check_login('tb_guru', array('nik_guru' => $nik_guru), array('password' => $password));
	            
	            //jika ditemukan, maka create session
	            if ($checking != FALSE) {
	                foreach ($checking as $apps) {

	                    $session_data = array(
	                        'nik_guru'   => $apps->nik_guru,
	                        'nama' => $apps->nama,
	                        'pass' => $apps->password,
	                        'level'=> $apps->level
	                        
	                    );
	                    //set session userdata
	                    $this->session->set_userdata($session_data);

	                    redirect('C_dashboard');

	                }
	            }else{
	            	$this->session->set_flashdata('notif','<div class="alert alert-warning">Maaf! nik_guru / Pasword anda tidak terdaftar</div>'); // Buat session flashdata
	            	redirect('C_login');
	            }

	        }
		}
	}
    
            	public function logout(){
				$this->session->sess_destroy(); // Hapus semua session
				redirect('C_login');  // Redirect ke halaman login
		}
}

