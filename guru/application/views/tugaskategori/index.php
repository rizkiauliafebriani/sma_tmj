<div id="page-wrapper">
          <div class="header"> 
                        <h1 class="page-header">
                            Data Pengolah Kategori Tugas
                        </h1>
                        <ol class="breadcrumb">
                      <li class="active">Tabel Data Pengolah Kategori Tugas</li>
                    </ol> 
                                    
        </div>
<div id="page-inner">
  <?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>


            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->

                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <a href="<?php echo base_url(); ?>C_tugaskategori/tambah" class="btn btn-success float-right">+ Tugas Kategori</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                  <table class="table table-bordered table" id="dataTables-example">
                                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Kategori Tugas</th>                      
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php  $no = 1; foreach ( $varKategoriTugas as $u) : ?>
                    <tr>
                      <td><?= $no++; ?></td>
                      <td><?= $u['nama_kategori_tugas']; ?></td>
                      <td><center>
                        <a href="<?php echo base_url(); ?>C_tugaskategori/hapus/<?= $u['id_kategori_tugas'];?>" onclick="return confirm('Yakin untuk menghapus?');" class="btn-danger btn-sm"> Hapus</a>
                      </center></td>
                      </tr>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                </div>
                            
              </div>
            </div>
          </div>
        </div>
    