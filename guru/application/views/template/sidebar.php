<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
     <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Admin | SMA Taman Madya Jetis Yogyakarta</title>
    <!-- Bootstrap Styles-->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" />
  
    <!-- FontAwesome Styles-->
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="<?php echo base_url(); ?>assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="<?php echo base_url(); ?>assets/css/custom-styles.css" rel="stylesheet" />

    <!-- Google Fonts-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/dataTables/dataTables.bootstrap.css" >
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/Lightweight-Chart/cssCharts.css" > 
    
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https:///maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">

                <a class="navbar-brand" href="<?php echo base_url(); ?>index.html"><strong>bluebox</strong></a>
            </div>

            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a aria-expanded="false">
                        <h5><font color="black"> Tanggal Sekarang : <?php echo date('d F Y'); ?></font></h5>   
                    </a>
                </li>
                <li>
                    <a aria-expanded="false">
                         <h5> Selamat datang Admin <b>"<?php echo $this->session->userdata('username') ?>"</b></h5> 
                    </a> 
                </li>
                <li>
                    <a class="dropdown-toggle"  href="<?php echo site_url('C_login/logout'); ?>" onclick="return confirm('Yakin untuk Sign out?');">
                        <i class="fa fa-user fa-fw"></i><b>Sign Out</b>
                    </a>
                </li>
                <li>
                    <a class="dropdown-toggle"  href="<?php echo site_url('C_login/logout'); ?>" onclick="return confirm('Yakin untuk Sign out?');">
                    </a>
                </li>
            </ul>
        </nav>
        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                    <li>
                        <a href="<?php echo base_url('C_beranda'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('C_admin'); ?>"><i class="fa fa-desktop"></i> Data Admin</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('C_guru'); ?>"><i class="fa fa-bar-chart-o"></i> Data Guru</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('C_siswa'); ?>"><i class="fa fa-qrcode"></i> Data Siswa</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('C_jurusan'); ?>"><i class="fa fa-table"></i> Data Jurusan</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('C_kelas'); ?>"><i class="fa fa-table"></i> Data Kelas</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('C_mapel'); ?>"><i class="fa fa-table"></i> Data Mapel</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('C_detailmapel'); ?>"><i class="fa fa-table"></i> Data Detail Mapel</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url(); ?>C_berita" ><i class="fa fa-table"></i> Data Berita</a>
                    </li>
                </ul>

            </div>

        </nav>
        </div>
     