<script type="text/javascript" src="<?php echo base_url(); ?>../admin/assets/ckeditor/ckeditor.js"></script>
<div id="page-wrapper" >
      <div class="header"> 
                        <h1 class="page-header">
                             Data Pengolah Diskusi
                        </h1>
            <ol class="breadcrumb">
            <li class="active">Tambah Diskusi</li>
          </ol> 
                  
    </div>
    
            <div id="page-inner"> 
              <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <a href="<?php echo base_url('C_diskusi/index'); ?>" class="btn btn-warning float-right"><< Kembali</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
<?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>
                                <div class="col-lg-6">
                                    <form action="<?php echo site_url('C_diskusi/tambah_aksi'); ?>" method="post" enctype="multipart/form-data">
                                    <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Topik Diskusi</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" name="topik_diskusi" class="form-control" value="<?php echo set_value('topik_diskusi')?>" required><small><span class="text-danger"><?php echo form_error('topik_diskusi'); ?></span></small>
                                    </div>
                                </div>
                                <div class="form-group row" readonly>
                                            <div class="col-12 col-sm-12 col-lg-12">
                                              <label>Isi Diskusi</label>
                                                <textarea style="width:500px; height:500px" class="ckeditor" id="ckedtor" name="isi_diskusi" value="<?php echo set_value('isi_diskusi')?>" required><small><span class="text-danger"><?php echo form_error('isi_diskusi'); ?></span></small></textarea>
                                            </div>
                                    </div>
                                <div class="row form-group">
                                <div class="col col-md-3">
                                    <label class=" form-control-label">Kelas</label>
                                </div>
                                <div class="col-6 col-md-9">
                                <select class="form-control" name="id_kelas" id="id_kelas"  onclick='validasi("id_kelas","nama_kelas")' required>
                                    <option>-Pilih Kelas-</option>
                                    <?php foreach ($varKelas as $pro) :  ?>
                                    <option value="<?php echo $pro['id_kelas']?>">
                                    <?php echo $pro['nama_kelas']?></option>
                                    <?php endforeach; ?>
                     
                                </select>
                                </div>
                            </div>
                            <div class="row form-group">
                             <div class="col col-md-3">
                                <label class=" form-control-label">Jurusan</label>
                            </div>
                            <div class="col-6 col-md-9">
                            <select class="form-control" name="id_jurusan" id="id_jurusan"  onclick='validasi("id_jurusan","nama_jurusan")' required>
                                <option>-Pilih Jurusan-</option>
                                    <?php foreach ($varJurusan as $pro) :  ?>
                                <option value="<?php echo $pro['id_jurusan']?>">
                                    <?php echo $pro['nama_jurusan']?></option>
                                    <?php endforeach; ?>
                     
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Pengajar</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" name="nama_guru" class="form-control" value="<?php echo $this->session->userdata('nik_guru') ?>" required><small><span class="text-danger"><?php echo form_error('nama_guru'); ?></span></small>
                                    </div>
                                </div>
                                <div class="col-md-6 offset-md-3">
                                      <input type="submit" value="Simpan" class="btn btn-warning float-right">
                                </div>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
     