<?php

class M_tugaskategori extends CI_Model{

	public function getTugasKategori ()
	{
	return $query = $this->db->get('tb_kategori_tugas')->result_array();
	} 

	public function input_data($data){
		$data = array(
	  	  	"nama_kategori_tugas" => $this->input->post('nama_kategori_tugas', true)
   			 );
      	return $this->db->insert('tb_kategori_tugas',$data);

		}
	public function hapus_data($id_kategori_tugas){
		$this->db->where('id_kategori_tugas',$id_kategori_tugas);
		$this->db->delete('tb_kategori_tugas');
	}
}