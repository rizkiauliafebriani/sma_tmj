-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 18, 2020 at 09:05 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sma_tmj`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `nik_admin` varchar(50) NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `email` varchar(40) NOT NULL,
  `foto` varchar(40) NOT NULL DEFAULT 'default.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `username`, `password`, `nik_admin`, `nama_admin`, `email`, `foto`) VALUES
(1, 'admin', '827ccb0eea8a706c4c34a16891f84e7b', '54321', 'Adminiyah', 'adminiyah@gmail.com', 'default.jpg'),
(3, 'adminnew', '827ccb0eea8a706c4c34a16891f84e7b', '1234321', 'admin newbie nih', 'adminnewbie@gmail.com', 'default.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_berita`
--

CREATE TABLE `tb_berita` (
  `id_berita` int(11) NOT NULL,
  `judul_berita` varchar(100) NOT NULL,
  `isi_berita` text NOT NULL,
  `waktu_upload` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_berita`
--

INSERT INTO `tb_berita` (`id_berita`, `judul_berita`, `isi_berita`, `waktu_upload`) VALUES
(10, 'Pendaftaran siswa baru TA 2020/21', '<p>Pendaftaran siswa baru TA 2020/2021 dibuka mulai tanggal 20 Mei 2020. Berbibit unggul dan gembira adalah motto SMA Taman Madya Yogyakarta</p>\r\n', '2020-10-14 10:01:21'),
(11, 'Juara 2 Lomba Olympiade tingkat Daerah', '<p>Jolie Agata dan timnya (Andika R, Putri Siska, Doni Ardianto) siswa kela X IPA telah berhasil menjuarai Lomba Olympiade Kimia Tingkat Daerah. Hal ini merupakan suatu pencapain yang tidak siasia</p>\r\n', '2020-10-14 10:04:09');

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_mapel`
--

CREATE TABLE `tb_detail_mapel` (
  `id_detail_mapel` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `nik_guru` char(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_detail_mapel`
--

INSERT INTO `tb_detail_mapel` (`id_detail_mapel`, `id_mapel`, `id_kelas`, `id_jurusan`, `nik_guru`) VALUES
(1, 3, 33, 2, '0000001'),
(2, 4, 34, 4, '0000001');

-- --------------------------------------------------------

--
-- Table structure for table `tb_diskusi`
--

CREATE TABLE `tb_diskusi` (
  `id_diskusi` int(11) NOT NULL,
  `topik_diskusi` varchar(100) NOT NULL,
  `isi_diskusi` text NOT NULL,
  `nik_guru` char(35) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_jurusan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_guru`
--

CREATE TABLE `tb_guru` (
  `nik_guru` char(35) NOT NULL,
  `nama_guru` varchar(100) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(32) NOT NULL,
  `agama` varchar(32) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `email` varchar(40) NOT NULL,
  `telepon` varchar(12) DEFAULT NULL,
  `foto` varchar(40) DEFAULT 'default.jpg',
  `level` enum('guru') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_guru`
--

INSERT INTO `tb_guru` (`nik_guru`, `nama_guru`, `password`, `jenis_kelamin`, `agama`, `alamat`, `email`, `telepon`, `foto`, `level`) VALUES
('0000001', 'guru gatra', '25d55ad283aa400af464c76d713c07ad', 'laki-laki', 'islam', 'alamat palsu', 'guru@gmail.com', '089543264563', 'default.jpg', 'guru');

-- --------------------------------------------------------

--
-- Table structure for table `tb_home`
--

CREATE TABLE `tb_home` (
  `id_home` int(11) NOT NULL,
  `judul_home` varchar(100) NOT NULL,
  `isi_home` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_home`
--

INSERT INTO `tb_home` (`id_home`, `judul_home`, `isi_home`) VALUES
(1, 'SMA TAMAN MADYA JETIS YOGYAKARTA', '<h3>SMA TAMAN MADYA JETIS  adalah bagian dari Yayasan Persatuan Perguruan Tamansiswa yang merupakan Yayasan yang berwawasan kebangsaan, merangkul siswa dari semua golongan, suku, ras dan agama. Pendidikan Budi Pekerti, muatan lokal, Pendidikan Kewirausahaan dan Pendidikan Karakter Bangsa : Jujur, bertanggungjawab, toleransi, kerja keras, komunikatif, religus dan rasa ingin tahu, merupakan terobosan yang  diutamakan di sekolah ini. Dengan metode Pengajaran Berbasis TIK peserta didik benar-benar dapat merencanakan studi dengan sebaik-baiknya.</h3>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jurusan`
--

CREATE TABLE `tb_jurusan` (
  `id_jurusan` int(11) NOT NULL,
  `nama_jurusan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_jurusan`
--

INSERT INTO `tb_jurusan` (`id_jurusan`, `nama_jurusan`) VALUES
(1, 'IPA'),
(2, 'IPS'),
(4, 'BAHASA');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori_tugas`
--

CREATE TABLE `tb_kategori_tugas` (
  `id_kategori_tugas` int(11) NOT NULL,
  `nama_kategori_tugas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_kategori_tugas`
--

INSERT INTO `tb_kategori_tugas` (`id_kategori_tugas`, `nama_kategori_tugas`) VALUES
(1, 'Tugas 1'),
(2, 'Tugas 2'),
(3, 'Tugas 3'),
(4, 'Tugas 4'),
(5, 'Tugas 5');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kelas`
--

CREATE TABLE `tb_kelas` (
  `id_kelas` int(11) NOT NULL,
  `nama_kelas` varchar(30) NOT NULL,
  `id_jurusan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_kelas`
--

INSERT INTO `tb_kelas` (`id_kelas`, `nama_kelas`, `id_jurusan`) VALUES
(8, 'X-IPA 1', 1),
(9, 'X-IPA 2', 1),
(10, 'X-IPA 3', 1),
(11, 'X-IPA 4', 1),
(12, 'X-IPA 5', 1),
(13, 'X-IPS 1', 2),
(14, 'X-IPS 2', 2),
(15, 'X-IPS 3', 2),
(16, 'X-BAHASA', 4),
(17, 'XI-IPA 1', 1),
(18, 'XI-IPA 2', 1),
(19, 'XI-IPA 3', 1),
(20, 'XI-IPA 4', 1),
(21, 'XI-IPA 5', 1),
(22, 'XI-IPS 1', 2),
(23, 'XI-IPS 2', 2),
(24, 'XI-IPS 3', 2),
(25, 'XI-BAHASA', 4),
(26, 'XII-IPA 1', 1),
(27, 'XII-IPA 2', 1),
(28, 'XII-IPA 3', 1),
(29, 'XII-IPA 4', 1),
(30, 'XII-IPA 5', 1),
(31, 'XII-IPS 1', 2),
(32, 'XII-IPS 2', 2),
(33, 'XII-IPS 3', 2),
(34, 'XII-BAHASA', 4);

-- --------------------------------------------------------

--
-- Table structure for table `tb_mapel`
--

CREATE TABLE `tb_mapel` (
  `id_mapel` int(11) NOT NULL,
  `nama_mapel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_mapel`
--

INSERT INTO `tb_mapel` (`id_mapel`, `nama_mapel`) VALUES
(1, 'Agama Islam'),
(3, 'Matematika'),
(4, 'Bahasa Indonesia'),
(6, 'Bahasa Inggris');

-- --------------------------------------------------------

--
-- Table structure for table `tb_siswa`
--

CREATE TABLE `tb_siswa` (
  `id_siswa` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nis` char(20) NOT NULL,
  `nama_siswa` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(32) NOT NULL,
  `agama` varchar(32) NOT NULL,
  `alamat` text NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `foto` varchar(40) NOT NULL DEFAULT 'default.jpg',
  `level` enum('siswa') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_siswa`
--

INSERT INTO `tb_siswa` (`id_siswa`, `username`, `password`, `nis`, `nama_siswa`, `jenis_kelamin`, `agama`, `alamat`, `id_kelas`, `id_jurusan`, `foto`, `level`) VALUES
(1, 'siswa1', '25d55ad283aa400af464c76d713c07ad', '11111', 'siswa coba satu', 'perempuan', 'islam', 'godean', 1, 1, 'default.jpg', 'siswa'),
(2, 'siswa2', '12345678', '0009', 'Andini Rika Purti', 'perempuan', 'islam', 'Gg.Gentan no 34 , Nologaten ', 1, 1, 'default.jpg', 'siswa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `tb_berita`
--
ALTER TABLE `tb_berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `tb_detail_mapel`
--
ALTER TABLE `tb_detail_mapel`
  ADD PRIMARY KEY (`id_detail_mapel`),
  ADD KEY `id_mapel` (`id_mapel`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_jurusan` (`id_jurusan`),
  ADD KEY `nik_guru` (`nik_guru`);

--
-- Indexes for table `tb_diskusi`
--
ALTER TABLE `tb_diskusi`
  ADD PRIMARY KEY (`id_diskusi`),
  ADD KEY `nik_guru` (`nik_guru`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_jurusan` (`id_jurusan`);

--
-- Indexes for table `tb_guru`
--
ALTER TABLE `tb_guru`
  ADD PRIMARY KEY (`nik_guru`);

--
-- Indexes for table `tb_home`
--
ALTER TABLE `tb_home`
  ADD PRIMARY KEY (`id_home`);

--
-- Indexes for table `tb_jurusan`
--
ALTER TABLE `tb_jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `tb_kategori_tugas`
--
ALTER TABLE `tb_kategori_tugas`
  ADD PRIMARY KEY (`id_kategori_tugas`);

--
-- Indexes for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  ADD PRIMARY KEY (`id_kelas`),
  ADD KEY `id_jurusan` (`id_jurusan`);

--
-- Indexes for table `tb_mapel`
--
ALTER TABLE `tb_mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD PRIMARY KEY (`id_siswa`),
  ADD KEY `jenis_kelamin` (`jenis_kelamin`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_jurusan` (`id_jurusan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_berita`
--
ALTER TABLE `tb_berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tb_detail_mapel`
--
ALTER TABLE `tb_detail_mapel`
  MODIFY `id_detail_mapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_diskusi`
--
ALTER TABLE `tb_diskusi`
  MODIFY `id_diskusi` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_home`
--
ALTER TABLE `tb_home`
  MODIFY `id_home` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_jurusan`
--
ALTER TABLE `tb_jurusan`
  MODIFY `id_jurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_kategori_tugas`
--
ALTER TABLE `tb_kategori_tugas`
  MODIFY `id_kategori_tugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `tb_mapel`
--
ALTER TABLE `tb_mapel`
  MODIFY `id_mapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
