<div id="page-wrapper">
          <div class="header"> 
                        <h1 class="page-header">
                            Data siswa
                        </h1>
                        <ol class="breadcrumb">
                      <li class="active">Tabel Data Pengolah siswa</li>
                    </ol> 
                                    
        </div>
<div id="page-inner">
  <?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>

            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <a href="<?php echo base_url('C_siswa/tambah'); ?>" class="btn btn-success float-right">Tambah siswa</a>
                        </div><hr>
                        <div class="panel-body">
                            <div class="table-responsive">
                             <table class="table table-bordered table" id="dataTables-example">
                                    <thead>
                    <tr>
                      <th>Nis</th>
                      <th>Nama</th>
                      <th>Username</th>
                      <th>Jenis Kelamin</th>
                      <th>Alamat</th>
                      <th>Kelas</th>
                      <th>Jurusan</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                     <?php  foreach ( $varSiswa as $u) : ?>
                    <tr>
                      <td><?= $u['nis']; ?></td>
                      <td><?= $u['nama_siswa']; ?></td>
                      <td><?= $u['username']; ?></td>
                      <td><?= $u['jenis_kelamin']; ?></td>
                      <td><?= $u['alamat']; ?></td>
                      <td><?= $u['id_kelas']; ?></td>
                      <td><?= $u['id_jurusan']; ?></td>
                      <td><center>
                        <a href="<?php echo base_url(); ?>C_siswa/hapus/<?= $u['id_siswa'];?>" onclick="return confirm('Yakin untuk menghapus?');" class="btn-danger btn-sm"> Hapus</a>
                      </center></td>
                    </tr>
                    <?php endforeach; ?>
                                    </tbody>

                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        
