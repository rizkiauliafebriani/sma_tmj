<div id="page-wrapper" >
      <div class="header"> 
                        <h1 class="page-header">
                             Data Pengolah siswa
                        </h1>
            <ol class="breadcrumb">
            <li class="active">Tambah guru</li>
          </ol> 
                  
    </div>
    
            <div id="page-inner"> 
              <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <a href="<?php echo base_url('C_siswa/index'); ?>" class="btn btn-warning float-right"><< Kembali</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
<?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>
<div class="col-lg-6">
                                    <form action="<?php echo site_url('C_siswa/tambah_aksi'); ?>" method="post" enctype="multipart/form-data">
                                        <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Nis</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="nis" class="form-control" value="<?php echo set_value('nis')?>" required><small><span class="text-danger"><?php echo form_error('nis'); ?></span></small>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Nama Siswa</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="nama_siswa" class="form-control" value="<?php echo set_value('nama_siswa')?>" required><small><span class="text-danger"><?php echo form_error('nama_siswa'); ?></span></small>
                                    </div>
                                </div>
                          
                                 <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Jenis Kelamin</label>
                                    </div>
                                <div class="col-12 col-md-6">
                                <select class="form-control" name="jenis_kelamin" id="jenis_kelamin"  onclick='validasi("jenis_kelamin","jenis_kelamin")' required>
                                    <option>-Pilih Jenis Kelamin-</option>
                                    <option value="laki-laki">Laki-Laki</option>
                                    <option value="perempuan">Perempuan</option> 
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Agama</label>
                                    </div>
                                <div class="col-12 col-md-6">
                                <select class="form-control" name="agama" id="agama"  onclick='validasi("agama","agama")' required>
                                    <option>-Pilih Agama-</option>
                                    <option value="islam">Islam</option>
                                    <option value="kristen">Kristen</option> 
                                    <option value="katholik">Katholik</option>
                                    <option value="hindu">Hindu</option>
                                    <option value="budha">Budha</option>
                                    <option value="lainnya">Lainnya</option>    
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">alamat</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <textarea type="text" name="alamat" class="form-control" value="<?php echo set_value('alamat')?>" required></textarea> 
                                    </div>
                                </div>
                                <div class="row form-group">
                <div class="col col-md-3">
                  <label class=" form-control-label">Kelas</label>
                </div>
                <div class="col-12 col-md-6">
                   <select class="form-control" name="id_kelas" id="id_kelas"  onclick='validasi("id_kelas","nama_kelas")' required>
                    <option>-Pilih Kelas-</option>
                    <?php foreach ($varKelas as $pro) :  ?>
                      <option value="<?php echo $pro['id_kelas']?>">
                        <?php echo $pro['nama_kelas']?></option>
                      <?php endforeach; ?>
                     
                   </select>
                </div>
              </div>
              <div class="row form-group">
                <div class="col col-md-3">
                  <label class=" form-control-label">Jurusan</label>
                </div>
                <div class="col-12 col-md-6">
                   <select class="form-control" name="id_jurusan" id="id_jurusan"  onclick='validasi("id_jurusan","nama_jurusan")' required>
                    <option>-Pilih Jurusan-</option>
                    <?php foreach ($varJurusan as $pro) :  ?>
                      <option value="<?php echo $pro['id_jurusan']?>">
                        <?php echo $pro['nama_jurusan']?></option>
                      <?php endforeach; ?>
                     
                   </select>
                </div>
              </div>
              <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Foto</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="foto" class="form-control" value="<?php echo set_value('foto')?>" required><small><span class="text-danger"><?php echo form_error('foto'); ?></span></small>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Level</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="level" class="form-control" value="<?php echo set_value('level')?>" required><small><span class="text-danger"><?php echo form_error('level'); ?></span></small>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Username</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="username" class="form-control" value="<?php echo set_value('username')?>" required><small><span class="text-danger"><?php echo form_error('username'); ?></span></small>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Password</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="password" class="form-control" value="<?php echo set_value('password')?>" required><small><span class="text-danger"><?php echo form_error('password'); ?></span></small>
                                    </div>
                                </div>
                                <div class="col-md-6 offset-md-3">
                                      <input type="submit" value="Simpan" class="btn btn-warning float-right">
                                </div>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
