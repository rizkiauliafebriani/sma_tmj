<div id="page-wrapper">
          <div class="header"> 
                        <h1 class="page-header">
                            Data mapel
                        </h1>
                        <ol class="breadcrumb">
                      <li class="active">Tabel Data Pengolah mapel</li>
                    </ol> 
                                    
        </div>
<div id="page-inner">
  <?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>

            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <a href="<?php echo base_url('C_mapel/tambah'); ?>" class="btn btn-warning float-right">Tambah mapel</a>
                        </div><hr>
                        <div class="panel-body">
                            <div class="table-responsive">
                             <table class="table table-bordered table" id="dataTables-example">
                                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Mata Pelajaran</th>                    
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                     <?php  $no = 1; foreach ( $varMapel as $u) : ?>
                    <tr>
                      <td><?= $no; ?></td>
                      <td><?= $u['nama_mapel']; ?></td>
                      <td><center>
                        <a href="<?php echo base_url(); ?>C_mapel/hapus/<?= $u['id_mapel'];?>" onclick="return confirm('Yakin untuk menghapus?');" class="btn-danger btn-sm"> Hapus</a>
                      </center></td>
                    </tr>
                    <?php $no++; ?>
                    <?php endforeach; ?>
                                    </tbody>

                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        
