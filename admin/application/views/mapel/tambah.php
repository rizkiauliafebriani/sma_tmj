<div id="page-wrapper" >
      <div class="header"> 
                        <h1 class="page-header">
                             Data Pengolah mapel
                        </h1>
            <ol class="breadcrumb">
            <li class="active">Tambah mapel</li>
          </ol> 
                  
    </div>
    
            <div id="page-inner"> 
              <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <a href="<?php echo base_url('C_mapel/index'); ?>" class="btn btn-warning float-right"><< Kembali</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
<?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>
                                <div class="col-lg-6">
                                    <form action="<?php echo site_url('C_mapel/tambah_aksi'); ?>" method="post" enctype="multipart/form-data">
                                        <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">nama_mapel</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="nama_mapel" class="form-control" value="<?php echo set_value('nama_mapel')?>" required><small><span class="text-danger"><?php echo form_error('nama_mapel'); ?></span></small>
                                    </div>
                                </div>
                                <div class="col-md-6 offset-md-3">
                                      <input type="submit" value="Simpan" class="btn btn-warning float-right">
                                </div>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
     