<div id="page-wrapper">
          <div class="header"> 
                        <h1 class="page-header">
                            Data Pengolah Berita
                        </h1>
                        <ol class="breadcrumb">
                      <li class="active">Tabel Data Pengolah Berita</li>
                    </ol> 
                                    
        </div>
<div id="page-inner">
  <?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>

            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->

                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <a href="<?php echo base_url(); ?>C_berita/tambah" class="btn btn-warning float-right">+ Berita</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                  <table class="table table-bordered table" id="dataTables-example">
                                    <thead>
                    <tr>
                      <th>Judul Berita</th>
                      <th>Isi Berita</th>
                      <th>Waktu Upload</th>                      
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                     <?php  foreach ( $varBerita as $u) : ?>
                    <tr>
                      <td><?= $u['judul_berita']; ?></td>
                      <td><?php echo substr($u['isi_berita'], 0,200); ?></td>
                      <td><?= $u['waktu_upload']; ?></td>
                      <td><center>
                        <a href="<?php echo base_url(); ?>C_berita/hapus/<?= $u['id_berita'];?>" onclick="return confirm('Yakin untuk menghapus?');" class="btn-danger btn-sm"> Hapus</a>
                      </center></td>
                    </tr>
                    <?php endforeach; ?>
                                    </tbody>

                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
    