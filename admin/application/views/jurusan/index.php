<div id="page-wrapper">
          <div class="header"> 
                        <h1 class="page-header">
                            Data jurusan
                        </h1>
                        <ol class="breadcrumb">
                      <li class="active">Tabel Data Pengolah jurusan</li>
                    </ol> 
                                    
        </div>
<div id="page-inner">
  <?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>

            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <a href="<?php echo base_url('C_jurusan/tambah'); ?>" class="btn btn-warning float-right">Tambah jurusan</a>
                        </div><hr>
                        <div class="panel-body">
                            <div class="table-responsive">
                             <table class="table table-bordered table" id="dataTables-example">
                                    <thead>
                    <tr>
                      <th>Nama Jurusan</th>                      
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                     <?php  foreach ( $varJurusan as $u) : ?>
                    <tr>
                      <td><?= $u['nama_jurusan']; ?></td>
                      <td><center>
                        <a href="<?php echo base_url(); ?>C_jurusan/hapus/<?= $u['id_jurusan'];?>" onclick="return confirm('Yakin untuk menghapus?');" class="btn-danger btn-sm"> Hapus</a> 
                        <!--a href="<?php echo base_url(); ?>C_jurusan/ubah/<?= $u['id_jurusan'];?>" class="btn-danger btn-sm"> Edit</a-->
                      </center></td>
                    </tr>
                    <?php endforeach; ?>
                                    </tbody>

                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        
