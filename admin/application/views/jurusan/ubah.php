<div id="page-wrapper" >
      <div class="header"> 
                        <h1 class="page-header">
                             Data Pengolah Jurusan
                        </h1>
            <ol class="breadcrumb">
            <li class="active">Edit Jurusan</li>
          </ol> 
                  
    </div>
    
            <div id="page-inner"> 
              <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <a href="<?php echo base_url('C_jurusan'); ?>" class="btn btn-warning float-right"><< Kembali</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
<?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>
                                <div class="col-lg-6">
                                    <form action="" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="id_jurusan" value="<?= $jurusanId['id_jurusan'];?>">
                                        <div class="row form-group">
                                    <div class="col col-md-3">
                                        <input type="hidden" name="id_jurusan" value="<?= $jurusanId['id_jurusan'];?>">
                                        <label class=" form-control-label">nama_jurusan</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="nama_jurusan" placeholder="nama_jurusan" class="form-control"  value="<?= $jurusanId['nama_jurusan'];?>" required minlength="3" maxlength="15">
                                    </div>
                                </div>
                                <div class="col-md-6 offset-md-3">
                                      <input type="submit" value="simpan" class="btn btn-warning float-right">
                                </div>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
     