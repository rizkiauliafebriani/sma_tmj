<div id="page-wrapper" >
      <div class="header"> 
                        <h1 class="page-header">
                             Data Pengolah Admin
                        </h1>
            <ol class="breadcrumb">
            <li class="active">Tambah Admin</li>
          </ol> 
                  
    </div>
    
            <div id="page-inner"> 
              <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <a href="<?php echo base_url('C_admin/indexjhj'); ?>" class="btn btn-success float-right"><< Kembali</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
<?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>
                                <div class="col-lg-6">
                                    <form role="form" action="<?php echo site_url('C_guru/tambah_aksi'); ?>" method="post" enctype="multipart/form-data">

                                         <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Nik Guru</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="nik_guru" class="form-control" value="<?php echo set_value('nik_guru')?>" required><small><span class="text-danger"><?php echo form_error('nik_guru'); ?></span></small>
                                    </div>
                                </div>
                                 <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Password</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="password" class="form-control" required><small><span class="text-danger"><?php echo form_error('password'); ?></span></small>
                                    </div>
                                </div>
                                 <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Nama guru</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="nama_guru" class="form-control" value="<?php echo set_value('nama_guru')?>" required><small><span class="text-danger"><?php echo form_error('nama_guru'); ?></span></small>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Email</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="email" class="form-control" value="<?php echo set_value('email')?>" required><small><span class="text-danger"><?php echo form_error('email'); ?></span></small>
                                    </div>
                                </div>                                       
                                        
                                        <button type="submit" class="btn btn-default">Submit Button</button>
                                        <button type="reset" class="btn btn-default">Reset Button</button>
                                    </form>
                                </div>
                                <!-------------------- /.col-lg-6 (nested) ------------->
                                <div class="col-lg-6">      
                                    <div class="form-group">                              
                                        <div class="col-12 col-lg-6">
                                            <label>Selects</label>
                                            <select class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                
                                        <div class="col-12 col-lg-6">
                                            <label>Selects</label>
                                            <select class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                            <label>Text Input with Placeholder</label>
                                            <input class="form-control" placeholder="Enter text">
                                </div>
                                </div>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
     


      <div id="page-inner"> 
              <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Basic Form Elements
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form">
                                    
                                        
                                       
                               
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            