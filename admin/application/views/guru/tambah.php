<div id="page-wrapper" >
      <div class="header"> 
                        <h1 class="page-header">
                             Data Pengolah guru
                        </h1>
            <ol class="breadcrumb">
            <li class="active">Tambah guru</li>
          </ol> 
                  
    </div>
    
            <div id="page-inner"> 
              <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <a href="<?php echo base_url('C_guru/index'); ?>" class="btn btn-warning float-right"><< Kembali</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
<?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>
                                <div class="col-lg-6">
                                    <form action="<?php echo site_url('C_guru/tambah_aksi'); ?>" method="post" enctype="multipart/form-data">
                                        <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">nik_guru</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="nik_guru" class="form-control" value="<?php echo set_value('nik_guru')?>" required><small><span class="text-danger"><?php echo form_error('nik_guru'); ?></span></small>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">nama_guru</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="nama_guru" class="form-control" value="<?php echo set_value('nama_guru')?>" required><small><span class="text-danger"><?php echo form_error('nama_guru'); ?></span></small>
                                    </div>
                                </div>
                                 <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Password</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="password" class="form-control" required><small><span class="text-danger"><?php echo form_error('password'); ?></span></small>
                                    </div>
                                </div>
                                 <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Jenis Kelamin</label>
                                    </div>
                                <div class="col-12 col-md-6">
                                <select class="form-control" name="jenis_kelamin" id="jenis_kelamin"  onclick='validasi("jenis_kelamin","jenis_kelamin")' required>
                                    <option>-Pilih Jenis Kelamin-</option>
                                    <option value="laki-laki">Laki-Laki</option>
                                    <option value="perempuan">Perempuan</option> 
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Agama</label>
                                    </div>
                                <div class="col-12 col-md-6">
                                <select class="form-control" name="agama" id="agama"  onclick='validasi("agama","agama")' required>
                                    <option>-Pilih Agama-</option>
                                    <option value="islam">Islam</option>
                                    <option value="kristen">Kristen</option> 
                                    <option value="katholik">Katholik</option>
                                    <option value="hindu">Hindu</option>
                                    <option value="budha">Budha</option>
                                    <option value="lainnya">Lainnya</option>    
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">alamat</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <textarea type="text" name="alamat" class="form-control" value="<?php echo set_value('alamat')?>" required></textarea> 
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Email</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="email" class="form-control" value="<?php echo set_value('email')?>" required><small><span class="text-danger"><?php echo form_error('email'); ?></span></small>
                                    </div>
                                </div>
                                
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Telepon</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="telepon" class="form-control" value="<?php echo set_value('telepon')?>" required><small><span class="text-danger"><?php echo form_error('telepon'); ?></span></small>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Foto</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="foto" class="form-control" value="<?php echo set_value('foto')?>" required><small><span class="text-danger"><?php echo form_error('foto'); ?></span></small>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label class=" form-control-label">Level</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" name="level" class="form-control" value="<?php echo set_value('level')?>" required><small><span class="text-danger"><?php echo form_error('level'); ?></span></small>
                                    </div>
                                </div>
                                <div class="col-md-6 offset-md-3">
                                      <input type="submit" value="Simpan" class="btn btn-warning float-right">
                                </div>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
     