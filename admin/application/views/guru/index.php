<div id="page-wrapper">
          <div class="header"> 
                        <h1 class="page-header">
                            Data Guru
                        </h1>
                        <ol class="breadcrumb">
                      <li class="active">Tabel Data Pengolah Guru</li>
                    </ol> 
                                    
        </div>
<div id="page-inner">
  <?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>

            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <a href="<?php echo base_url('C_guru/tambah'); ?>" class="btn btn-warning float-right">Tambah Guru</a>
                        </div><hr>
                        <div class="panel-body">
                            <div class="table-responsive">
                             <table class="table table-bordered table" id="dataTables-example">
                                    <thead>
                    <tr>
                      <th>Nik Guru</th>
                      <th>Nama</th>
                      <th>Jenis Kelamin</th> 
                      <th>Agama</th> 
                      <th>Telepon</th> 
                      <th>Email</th>   
                      <th>Alamat</th>                      
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                     <?php  foreach ( $varGuru as $u) : ?>
                    <tr>
                      <td><?= $u['nik_guru']; ?></td>
                      <td><?= $u['nama_guru']; ?></td>
                      <td><?= $u['jenis_kelamin']; ?></td>
                      <td><?= $u['agama']; ?></td>
                      <td><?= $u['telepon']; ?></td>
                      <td><?= $u['email']; ?></td>
                      <td><?= $u['alamat']; ?></td>
                      <td><center>
                        <a href="<?php echo base_url(); ?>C_guru/hapus/<?= $u['nik_guru'];?>" onclick="return confirm('Yakin untuk menghapus?');" class="btn-danger btn-sm"> Hapus</a>
                      </center></td>
                    </tr>
                    <?php endforeach; ?>
                                    </tbody>

                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        
