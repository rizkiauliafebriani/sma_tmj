<div id="page-wrapper">
          <div class="header"> 
                        <h1 class="page-header">
                            Data Detail Mapel
                        </h1>
                        <ol class="breadcrumb">
                      <li class="active">Tabel Data Pengolah Detail Mapel</li>
                    </ol> 
                                    
        </div>
<div id="page-inner">
  <?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>

            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <a href="<?php echo base_url('C_detailmapel/tambah'); ?> " class="btn btn-success float-right">Tambah Detail Mapel</a>
                        </div><hr>
                        <div class="panel-body">
                            <div class="table-responsive">
                  <table class="table table-bordered table" id="dataTables-example">
                                       <thead>
                                        <tr>
                                          <th>No</th>
                                          <th>MataPelajaran</th>
                                          <th>Kelas</th>
                                          <th>Jurusan</th>
                                          <th>Pengajar</th>
                                          <th>Aksi</th>
                                          
                                        </tr>
                                       </thead>
                                        <tbody>
                                        <?php  $no = 1; foreach ( $varDetail as $u) : ?>
                                        <tr class="gradeU">
                                          <td><?= $no++; ?></td>
                                          <td><?= $u['nama_mapel']; ?></td>
                                          <td><?= $u['nama_kelas']; ?></td>
                                          <td><?= $u['nama_jurusan']; ?></td>
                                          <td><?= $u['nama_guru']; ?></td>
                                          <td><center>
                                            <a href="<?php echo base_url(); ?>C_detailmapel/hapus/<?= $u['id_detail_mapel'];?>" onclick="return confirm('Yakin untuk menghapus?');" class="btn-danger btn-sm"> Hapus</a>
                                          </center></td>
                                        </tr>
                                     <?php endforeach; ?>
                                    </tbody>
                              </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        
