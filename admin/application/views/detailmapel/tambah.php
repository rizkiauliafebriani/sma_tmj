<div id="page-wrapper" >
    <div class="header"> 
        <h1 class="page-header">
          Data Pengolah Detail Mapel
        </h1>
        <ol class="breadcrumb">
          <li class="active">Tambah mapel</li>
        </ol>         
    </div>
    
    <div id="page-inner"> 
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <a href="<?php echo base_url('C_detailmapel/index'); ?>" class="btn btn-warning float-right"><< Kembali</a>
            </div>
          <div class="panel-body">
      <div class="row">
<?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>
          <div class="col-lg-6">
            <form action="<?php echo site_url('C_detailmapel/tambah_aksi'); ?>" method="post" enctype="multipart/form-data">
              <div class="row form-group">
                <div class="col col-md-3">
                  <label class=" form-control-label">Matapelajaran</label>
                </div>
                <div class="col-6 col-md-9">
                   <select class="form-control" name="id_mapel" id="id_mapel"  onclick='validasi("id_mapel","nama_mapel")' required>
                    <option>-Pilih Matapelajaran-</option>
                    <?php foreach ($varMapel as $pro) :  ?>
                      <option value="<?php echo $pro['id_mapel']?>">
                        <?php echo $pro['nama_mapel']?></option>
                      <?php endforeach; ?>
                     
                   </select>
                </div>
              </div>
              <div class="row form-group">
                <div class="col col-md-3">
                  <label class=" form-control-label">Kelas</label>
                </div>
                <div class="col-6 col-md-9">
                   <select class="form-control" name="id_kelas" id="id_kelas"  onclick='validasi("id_kelas","nama_kelas")' required>
                    <option>-Pilih Kelas-</option>
                    <?php foreach ($varKelas as $pro) :  ?>
                      <option value="<?php echo $pro['id_kelas']?>">
                        <?php echo $pro['nama_kelas']?></option>
                      <?php endforeach; ?>
                     
                   </select>
                </div>
              </div>
              <div class="row form-group">
                <div class="col col-md-3">
                  <label class=" form-control-label">Jurusan</label>
                </div>
                <div class="col-6 col-md-9">
                   <select class="form-control" name="id_jurusan" id="id_jurusan"  onclick='validasi("id_jurusan","nama_jurusan")' required>
                    <option>-Pilih Jurusan-</option>
                    <?php foreach ($varJurusan as $pro) :  ?>
                      <option value="<?php echo $pro['id_jurusan']?>">
                        <?php echo $pro['nama_jurusan']?></option>
                      <?php endforeach; ?>
                     
                   </select>
                </div>
              </div>
              <div class="row form-group">
                <div class="col col-md-3">
                  <label class=" form-control-label">Pengajar</label>
                </div>
                <div class="col-6 col-md-9">
                   <select class="form-control" name="nik_guru" id="nik_guru"  onclick='validasi("nik_guru","nama_guru")' required>
                    <option>-Pilih Pengajar-</option>
                    <?php foreach ($varGuru as $pro) :  ?>
                      <option value="<?php echo $pro['nik_guru']?>">
                        <?php echo $pro['nama_guru']?></option>
                      <?php endforeach; ?>
                     
                   </select>
                </div>
              </div>

              <div class="col-md-6 offset-md-3">
                <input type="submit" value="Simpan" class="btn btn-warning float-right">
              </div>
            </form>
          </div>
                                <!-- /.col-lg-6 (nested) -->
        </div>
                            <!-- /.row (nested) -->
      </div>
                        <!-- /.panel-body -->
    </div>
                    <!-- /.panel -->
  </div>
                <!-- /.col-lg-12 -->
</div>