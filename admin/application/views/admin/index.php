<div id="page-wrapper">
          <div class="header"> 
                        <h1 class="page-header">
                            Data Admin
                        </h1>
                        <ol class="breadcrumb">
                      <li class="active">Tabel Data Pengolah admin</li>
                    </ol> 
                                    
        </div>
<div id="page-inner">
  <?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>

            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <a href="<?php echo base_url('C_admin/tambah'); ?>" class="btn btn-warning float-right">Tambah admin</a>
                        </div><hr>
                        <div class="panel-body">
                            <div class="table-responsive">
                             <table class="table table-bordered table" id="dataTables-example">
                                    <thead>
                    <tr>
                      <th>Username</th>
                      <th>Nama Admin</th>
                      <th>Nik</th>                      
                      <th>Email</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                     <?php  foreach ( $varAdmin as $u) : ?>
                    <tr>
                      <td><?= $u['username']; ?></td>
                      <td><?= $u['nama_admin']; ?></td>
                      <td><?= $u['nik_admin']; ?></td>
                      <td><?= $u['email']; ?></td>
                      <td><center>
                        <a href="<?php echo base_url(); ?>C_admin/hapus/<?= $u['id_admin'];?>" onclick="return confirm('Yakin untuk menghapus?');" class="btn-danger btn-sm"> Hapus</a>
                      </center></td>
                    </tr>
                    <?php endforeach; ?>
                                    </tbody>

                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        
