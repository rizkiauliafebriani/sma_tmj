<div id="page-wrapper">
          <div class="header"> 
                        <h1 class="page-header">
                            Data Kelas
                        </h1>
                        <ol class="breadcrumb">
                      <li class="active">Tabel Data Pengolah Kelas</li>
                    </ol> 
                                    
        </div>
<div id="page-inner">
  <?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>

            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <a href="<?php echo base_url(); ?>C_kelas/tambah" class="btn btn-success float-right">Tambah Kelas</a>
                        </div><hr>
                        <div class="panel-body">
                            <div class="table-responsive">
                  <table class="table table-bordered table" id="dataTables-example">
                                       <thead>
                                        <tr>
                                          
                                          <th align="center">Nama Kelas</th>
                                          <th align="center">Nama Jurusan</th>
                                          <th align="center">Aksi</th>
                                          
                                        </tr>
                                       </thead>
                                        <tbody>
                                     <?php  foreach($varKelas as $u) : ?>
                                        <tr>
                                          <td><?= $u['nama_kelas']; ?></td>
                                          <td><?= $u['nama_jurusan']; ?></td>
                                          <td><center>
                                            <a href="<?php echo base_url(); ?>C_kelas/hapus/<?= $u['id_kelas'];?>" onclick="return confirm('Yakin untuk menghapus?');" class="btn-danger btn-sm"> Hapus</a>
                                          </center></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        
