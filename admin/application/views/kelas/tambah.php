<div id="page-wrapper" >
      <div class="header"> 
         <h1 class="page-header">
            Master Data Kelas
         </h1>
        <ol class="breadcrumb">
         <li class="active">Tambah Kelas</li>
        </ol>           
    </div>
    <div id="page-inner"> 
              <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <a href="<?php echo base_url('C_kelas/index'); ?>" class="btn btn-success float-right"><< Kembali</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                        <?php if($this->session->flashdata('notif')){
                            echo $this->session->flashdata('notif');
                        }?>
            <div class="col-lg-6">
            <form action="<?php echo site_url('C_kelas/tambah_aksi'); ?>" method="post" enctype="multipart/form-data">
              <div class="row form-group">
                <div class="col col-md-3">
                  <label class=" form-control-label">Nama Kelas</label>
                </div>
                <div class="col-6 col-md-9">
                   <input type="text" name="nama_kelas" placeholder="inputkan kelas" class="form-control" value="<?php echo set_value('nama_kelas')?>" required><small><span class="text-danger"><?php echo form_error('nama_kelas'); ?></span></small>
                </div>
              </div>
              <div class="row form-group">
                <div class="col col-md-3">
                  <label class=" form-control-label">Nama Jurusan</label>
                </div>
                <div class="col-6 col-md-9">
                   <select class="form-control" name="id_jurusan" id="id_jurusan"  onclick='validasi("id_jurusan","nama_jurusan")' required>
                    <option>-Pilih Jurusan-</option>
                    <?php foreach ($varJurusan as $pro) :  ?>
                      <option value="<?php echo $pro['id_jurusan']?>">
                        <?php echo $pro['nama_jurusan']?></option>
                      <?php endforeach; ?>
                     
                   </select>
                </div>
              </div>
              <div class="col-md-3 offset-md-4">
                <input class="form-control" align="center" type="submit" name="simpan" value="Simpan">
              </div>
            </form>
         </div>
           <!-- /.col-lg-6 (nested) -->
         </div>
         <!-- /.row (nested) -->
        </div>
        <!-- /.panel-body -->
        </div>
       <!-- /.panel -->
      </div>
    <!-- /.col-lg-12 -->
  </div>