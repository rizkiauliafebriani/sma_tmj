<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_jurusan extends CI_Controller {

public function index()
	{
		if($this->M_login->logged_id())
		{

		$data['varJurusan']= $this->M_jurusan->getJurusan();
		$this->load->view('template/sidebar');
		$this->load->view('jurusan/index', $data);
		$this->load->view('template/footer');

		}else{

			$this->session->set_flashdata('notif','<div class="alert alert-warning">Maaf! Silahkan Login Dahulu</div>');
			//jika session belum terdaftar, maka redirect ke halaman login
			redirect("C_login");

		}
	}
public function tambah(){	
		$this->load->view('template/sidebar');
		$this->load->view('jurusan/tambah');
		$this->load->view('template/footer');
	}

	
	function tambah_aksi()
			{ //validasi
		$this->form_validation->set_rules('nama_jurusan', 'nama_jurusan', 'required');
		if ( $this->form_validation->run() === FALSE ) {
			$this->load->view('template/sidebar');
			$this->load->view('jurusan/tambah');
			$this->load->view('template/footer');
		}else{
//input tb
			$data = [
				'nama_jurusan' => $this->input->post('nama_jurusan')
			];
				
			$insert = $this->M_jurusan->input_data($data);
			

			if ($insert) {
			$this->session->set_flashdata('notif','<div class="alert alert-warning">jurusan berhasil ditambah</div>');
				redirect('C_jurusan/index');
			} else {
			$this->session->set_flashdata('notif','<div class="alert alert-warning">Terjadi Kesalahan saat regisrasi</div>');
				redirect('C_jurusan/index');
			}

		}
	}

	function hapus($id_jurusan){
			$where = array('id_jurusan' => $id_jurusan);
			$this->M_jurusan->hapus_data($id_jurusan,'dataa');
			redirect('C_jurusan/index');
			}

	public function ubah($id_jurusan)
	{
		$data['jurusanId']= $this->M_jurusan->getJurusanByID($id_jurusan);
		$this->form_validation->set_rules('nama_jurusan', 'nama_jurusan', 'required');
		if ($this->form_validation->run() == FALSE){
			$this->load->view('template/sidebar');
			$this->load->view('jurusan/ubah', $data);
			$this->load->view('template/footer');
		} else {

			$this->M_jurusan->ubahDatajurusan();
			$this->session->set_flashdata('notif','<div class="alert alert-warning">Data jurusan berhasil diubah!</div>'); // Buat session flashdata
			redirect('C_jurusan');
		}
	}

}