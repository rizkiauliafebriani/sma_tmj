<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_detailmapel extends CI_Controller {

public function index(){
		$tbl['varDetail']=$this->M_detailmapel->getDetailMapel();
		$this->load->view('template/sidebar');
		$this->load->view('detailmapel/index',$tbl);
		$this->load->view('template/footer');
	}

public function tambah()
		{	
			$tbl['varJurusan']=$this->M_jurusan->getJurusan();		//ambil id jurusan
			$tbl['varKelas']=$this->M_kelas->getKelas();		//ambil id kelass
			$tbl['varMapel']=$this->M_mapel->getMapel();		//ambil id mapel
			$tbl['varGuru']=$this->M_guru->getGuru();		//ambil nik guru
			$this->load->view('template/sidebar');
			$this->load->view('detailmapel/tambah', $tbl);
			$this->load->view('template/footer');

		}
function tambah_aksi()
			{ //validasi
		$tbl['varJurusan']=$this->M_jurusan->getJurusan();
		$tbl['varKelas']=$this->M_kelas->getKelas();
		$tbl['varMapel']=$this->M_mapel->getMapel();
		$tbl['varGuru']=$this->M_guru->getGuru();
		$this->form_validation->set_rules('id_jurusan', 'nama_jurusan', 'required');
		$this->form_validation->set_rules('id_kelas', 'nama_kelas', 'required');
		$this->form_validation->set_rules('id_mapel', 'nama_mapel', 'required');
		$this->form_validation->set_rules('nik_guru', 'nama_guru', 'required');	


if ( $this->form_validation->run() === FALSE ) {
			$this->load->view('template/sidebar');
			$this->load->view('detailmapel/tambah', $tbl);
			$this->load->view('template/footer');
		}else{
	 //input tb
		$data = [
 				'id_mapel' => $this->input->post('id_mapel'),
 				'id_kelas' => $this->input->post('id_kelas'),
 				'id_jurusan' => $this->input->post('id_jurusan'),
 				'nik_guru' => $this->input->post('nik_guru')
 				];

 		$insert = $this->M_detailmapel->input_data($data);

 		if ($insert) {
			$this->session->set_flashdata('notif','<div class="alert alert-warning">Detail Mapel berhasil ditambah</div>');
				redirect('C_detailmapel/index');
			} else {
			$this->session->set_flashdata('notif','<div class="alert alert-warning">Terjadi Kesalahan saat regisrasi</div>');
				redirect('C_detailmapel/index');
			}
		}
	}

}