<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_guru extends CI_Controller {

public function index()
	{
		if($this->M_login->logged_id())
		{

		$data['varGuru']= $this->M_guru->getGuru();
		$this->load->view('template/sidebar');
		$this->load->view('guru/index', $data);
		$this->load->view('template/footer');

		}else{

			$this->session->set_flashdata('notif','<div class="alert alert-warning">Maaf! Silahkan Login Dahulu</div>');
			//jika session belum terdaftar, maka redirect ke halaman login
			redirect("C_login");

		}
	}


public function tambah(){	
		$this->load->view('template/sidebar');
		$this->load->view('guru/tambah');
		$this->load->view('template/footer');
	}

	
	function tambah_aksi()
			{ //validasi
		$this->form_validation->set_rules('nik_guru', 'nik_guru', 'required|min_length[5]|max_length[15]');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('nama_guru', 'nama_guru', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[15]');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'jenis_kelamin');
		$this->form_validation->set_rules('agama', 'agama');
		$this->form_validation->set_rules('telepon', 'telepon', 'required');

		if ( $this->form_validation->run() === FALSE ) {
			$this->load->view('template/sidebar');
			$this->load->view('guru/tambah');
			$this->load->view('template/footer');
		}else{
//input tb
			$data = [
				'alamat' => $this->input->post('alamat'),
				'telepon' => $this->input->post('telepon'),
				'agama' => $this->input->post('agama'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'nik_guru' => $this->input->post('nik_guru'),
				'nik_guru' => $this->input->post('nik_guru'),
				'email' => $this->input->post('email'),
				'password' =>md5($this->input->post('password')),
				'nama_guru' => $this->input->post('nama_guru')
			];
				
			$insert = $this->M_guru->input_data($data);
			

			if ($insert) {
			$this->session->set_flashdata('notif','<div class="alert alert-warning">guru berhasil ditambah</div>');
				redirect('C_guru/index');
			} else {
			$this->session->set_flashdata('notif','<div class="alert alert-warning">Terjadi Kesalahan saat regisrasi</div>');
				redirect('C_guru/index');
			}

		}
	}
		function hapus($nik_guru){
			$where = array('nik_guru' => $nik_guru);
			$this->M_guru->hapus_data($nik_guru,'dataa');
			redirect('C_guru/index');
			}

}