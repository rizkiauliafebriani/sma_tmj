<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_siswa extends CI_Controller {

public function index()
	{
		if($this->M_login->logged_id())
		{

		$data['varSiswa']= $this->M_siswa->getSiswa();
		$this->load->view('template/sidebar');
		$this->load->view('siswa/index', $data);
		$this->load->view('template/footer');

		}else{

			$this->session->set_flashdata('notif','<div class="alert alert-warning">Maaf! Silahkan Login Dahulu</div>');
			//jika session belum terdaftar, maka redirect ke halaman login
			redirect("C_login");

		}
	}

	public function tambah(){	
		$this->load->view('template/sidebar');
		$this->load->view('siswa/tambah');
		$this->load->view('template/footer');
	}

		function hapus($id_siswa){
			$where = array('id_siswa' => $id_siswa);
			$this->M_siswa->hapus_data($id_siswa,'dataa');
			redirect('C_siswa/index');
			}
}