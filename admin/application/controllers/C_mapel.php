<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_mapel extends CI_Controller {

public function index()
	{
		if($this->M_login->logged_id())
		{

		$data['varMapel']= $this->M_mapel->getMapel();
		$this->load->view('template/sidebar');
		$this->load->view('mapel/index', $data);
		$this->load->view('template/footer');

		}else{

			$this->session->set_flashdata('notif','<div class="alert alert-warning">Maaf! Silahkan Login Dahulu</div>');
			//jika session belum terdaftar, maka redirect ke halaman login
			redirect("C_login");

		}
	}


public function tambah(){	
		$this->load->view('template/sidebar');
		$this->load->view('mapel/tambah');
		$this->load->view('template/footer');
	}

	
	function tambah_aksi()
			{ //validasi
		$this->form_validation->set_rules('nama_mapel', 'nama_mapel', 'required|min_length[3]|max_length[30]');

		if ( $this->form_validation->run() === FALSE ) {
			$this->load->view('template/sidebar');
			$this->load->view('mapel/tambah');
			$this->load->view('template/footer');
		}else{
//input tb
			$data = [
				'nama_mapel' => $this->input->post('nama_mapel')
			];
				
			$insert = $this->M_mapel->input_data($data);
			

			if ($insert) {
			$this->session->set_flashdata('notif','<div class="alert alert-warning">mapel berhasil ditambah</div>');
				redirect('C_mapel/index');
			} else {
			$this->session->set_flashdata('notif','<div class="alert alert-warning">Terjadi Kesalahan saat regisrasi</div>');
				redirect('C_mapel/index');
			}

		}
	}

	function hapus($id_mapel){
			$where = array('id_mapel' => $id_mapel);
			$this->M_mapel->hapus_data($id_mapel,'dataa');
			redirect('C_mapel/index');
			}
}