<?php

class C_home Extends CI_Controller{

	public function index()
	{
		if($this->M_login->logged_id())
		{

		$data['varHome']= $this->M_home->getHome();
		$this->load->view('template/sidebar');
		$this->load->view('home/index', $data);
		$this->load->view('template/footer');

		}else{

			$this->session->set_flashdata('notif','<div class="alert alert-warning">Maaf! Silahkan Login Dahulu</div>');
			//jika session belum terdaftar, maka redirect ke halaman login
			redirect("C_login");

		}
	}

	public function ubah($id_home)
	{
		$data['varHomeid']= $this->M_home->getID($id_home);
		$this->form_validation->set_rules('judul_home', 'Judul_home', 'required');
		$this->form_validation->set_rules('isi_home', 'Isi_home', 'required');
		if ($this->form_validation->run() == FALSE){
			$this->load->view('template/sidebar');
			$this->load->view('home/ubah', $data);
			$this->load->view('template/footer');
		} else {

			$this->M_home->ubahData();
			$this->session->set_flashdata('notif','<div class="alert alert-warning">Data Home berhasil diubah</div>');
			redirect('C_home');
		}
	}
	
}



	 
		
		