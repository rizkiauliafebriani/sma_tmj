<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_kelas extends CI_Controller {

public function index(){

		$tbl['varKelas']=$this->M_kelas->getKelas();
		$this->load->view('template/sidebar');
		$this->load->view('kelas/index', $tbl);
		$this->load->view('template/footer');
	}

public function tambah()
		{	
			$tbl['varJurusan']=$this->M_jurusan->getJurusan();		//ambil id jurusan
			$this->load->view('template/sidebar');
			$this->load->view('kelas/tambah', $tbl);
			$this->load->view('template/footer');
		}
function tambah_aksi()
			{ //validasi
		$tbl['varJurusan']=$this->M_jurusan->getJurusan();		//ambil id jurusan
		$this->form_validation->set_rules('nama_kelas', 'nama_kelas', 'required|max_length[15]');
		$this->form_validation->set_rules('id_jurusan', 'nama_jurusan', 'required');

		if ( $this->form_validation->run() === FALSE ) {
			$this->load->view('template/sidebar');
			$this->load->view('kelas/tambah', $tbl);
			$this->load->view('template/footer');
		}else{
//input tb
			$data = [
				'nama_kelas' => $this->input->post('nama_kelas'),
				'id_jurusan' => $this->input->post('id_jurusan')
			];
				
			$insert = $this->M_kelas->input_data($data);
			

			if ($insert) {
			$this->session->set_flashdata('notif','<div class="alert alert-warning">kelas berhasil ditambah</div>');
				redirect('C_kelas/index');
			} else {
			$this->session->set_flashdata('notif','<div class="alert alert-warning">Terjadi Kesalahan saat regisrasi</div>');
				redirect('C_kelas/index');
			}

		}
	}

	function hapus($id_kelas){
			$where = array('id_kelas' => $id_kelas);
			$this->M_kelas->hapus_data($id_kelas,'dataa');
			redirect('C_kelas/index');
			}
	
}