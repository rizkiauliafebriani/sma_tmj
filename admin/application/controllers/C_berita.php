<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_berita extends CI_Controller {


public function index(){

		$data['varBerita']= $this->M_berita->getBerita();
		$this->load->view('template/sidebar');
		$this->load->view('berita/index', $data);
		$this->load->view('template/footer');

	}

public function tambah()
		{	
			$this->load->view('template/sidebar');
			$this->load->view('berita/tambah');
			$this->load->view('template/footer');
		}

function tambah_aksi()
			{ //validasi
		$this->form_validation->set_rules('judul_berita', 'judul_berita', 'required|min_length[5]|max_length[50]');
		$this->form_validation->set_rules('isi_berita', 'isi_berita', 'required');

		if ( $this->form_validation->run() === FALSE ) {
			$this->load->view('template/sidebar');
			$this->load->view('berita/tambah');
			$this->load->view('template/footer');
		}else{
//input tb
			$data = [
				'judul_berita' => $this->input->post('judul_berita'),
				'isi_berita' => $this->input->post('isi_berita')
			];
				
			$insert = $this->M_berita->input_data($data);
			

			if ($insert) {
			$this->session->set_flashdata('notif','<div class="alert alert-warning">Berita berhasil ditambah</div>');
				redirect('C_berita/index');
			} else {
			$this->session->set_flashdata('notif','<div class="alert alert-warning">Terjadi Kesalahan</div>');
				redirect('C_berita/index');
			}

		}
	}
	

public function hapus($id_berita){
			$where = array('id_berita' => $id_berita);
			$this->M_berita->hapus_berita($id_berita,'dataa');
			$this->session->set_flashdata('notif','<div class="alert alert-warning">Data berita berhasil dihapus!</div>');
			redirect('C_berita');
	}

}