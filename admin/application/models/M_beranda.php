<?php

class M_beranda extends CI_Model{

  public function hitungJumlahSiswa()
  {   
    $query = $this->db->get('tb_siswa');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
    else
    {
      return 0;
    }
    }

  public function hitungJumlahGuru()
  {   
    $query = $this->db->get('tb_guru');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
    else
    {
      return 0;
    }
    }

  public function hitungJumlahKelas()
  {   
    $query = $this->db->get('tb_kelas');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
    else
    {
      return 0;
    }
    }

  public function hitungJumlahMapel()
  {   
    $query = $this->db->get('tb_Mapel');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
    else
    {
      return 0;
    }
    }

  

}
