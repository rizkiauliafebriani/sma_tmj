<?php

class M_detailmapel extends CI_Model{

	public function getDetailMapel ()
	{
		$this->db->select('*');
        $this->db->from('tb_detail_mapel');
        $this->db->join('tb_mapel', 'tb_mapel.id_mapel=tb_detail_mapel.id_mapel');
        $this->db->join('tb_kelas', 'tb_kelas.id_kelas=tb_detail_mapel.id_kelas');
        $this->db->join('tb_jurusan', 'tb_jurusan.id_jurusan=tb_detail_mapel.id_jurusan');
        $this->db->join('tb_guru', 'tb_guru.nik_guru=tb_detail_mapel.nik_guru');
        $this->db->order_by('tb_detail_mapel.id_detail_mapel', 'desc');
        $query = $this->db->get_where();
        return $query->result_array();
	} 

	public function input_data($data){
		$data = array(
			"id_mapel" => $this->input->post('id_mapel', true),
	  	  	"id_kelas" => $this->input->post('id_kelas', true),
	  	  	"id_jurusan" => $this->input->post('id_jurusan', true),
	  	  	"nik_guru" => $this->input->post('nik_guru', true)
   			 );
      	return $this->db->insert('tb_detail_mapel',$data);
      	}

	public function hapus_data($id_detail_mapel){
		$this->db->where('id_detail_mapel',$id_detail_mapel);
		$this->db->delete('tb_detail_mapel');
	}

}