<?php

class M_kelas extends CI_Model{

	public function getKelas ()
	{
		$this->db->select('*');
        $this->db->from('tb_kelas');
        $this->db->join('tb_jurusan', 'tb_jurusan.id_jurusan=tb_kelas.id_jurusan');
        $this->db->order_by('tb_kelas.id_kelas', 'desc');
        $query = $this->db->get_where();
        return $query->result_array();
	} 

	public function input_data($data){
		$data = array(
	  	  	"nama_kelas" => $this->input->post('nama_kelas', true),
	  	  	"id_jurusan" => $this->input->post('id_jurusan', true)
   			 );
      	return $this->db->insert('tb_kelas',$data);

		}

	public function hapus_data($id_kelas){
		$this->db->where('id_kelas',$id_kelas);
		$this->db->delete('tb_kelas');
	}

}