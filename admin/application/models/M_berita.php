<?php

class M_berita extends CI_Model{

	public function getBerita ()
	{
	return $query = $this->db->get('tb_berita')->result_array();
	} 

	public function input_data($data){
		date_default_timezone_set('Asia/Jakarta');
     	$now = date('Y-m-d H:i:s');
		$data = array(
	  	  	"judul_berita" => $this->input->post('judul_berita', true),
	  	  	"waktu_upload" => $now,
	        "isi_berita" => $this->input->post('isi_berita', true)
   			 );
      	return $this->db->insert('tb_berita',$data);

		}

	public function hapus_berita($id_berita){
        $this->db->where('id_berita',$id_berita);
        $this->db->delete('tb_berita');
    }

}

