<?php

class M_jurusan extends CI_Model{

	public function getJurusan ()
	{
	return $query = $this->db->get('tb_jurusan')->result_array();
	} 

	public function input_data($data){
		$data = array(
	  	  	"nama_jurusan" => $this->input->post('nama_jurusan', true)
   			 );
      	return $this->db->insert('tb_jurusan',$data);
		}

	public function hapus_data($id_jurusan){
		$this->db->where('id_jurusan',$id_jurusan);
		$this->db->delete('tb_jurusan');
	}

	public function ubahData()
	{
		$data = [
		"nama_jurusan" => $this->input->post('nama_jurusan', true)
		];
		
		$this->db->where('id_jurusan', $this->input->post('id_jurusan'));
		$this->db->update('tb_jurusan', $data);
	}

	public function getJurusanById($id_jurusan)
	{
		return $this->db->get_where('tb_jurusan', ['id_jurusan' => $id_jurusan])->row_array();
	}


}