<?php

class M_mapel extends CI_Model{

	public function getMapel ()
	{
	return $query = $this->db->get('tb_mapel')->result_array();
	} 

	public function input_data($data){
		$data = array(
	  	  	"nama_mapel" => $this->input->post('nama_mapel', true)
   			 );
      	return $this->db->insert('tb_mapel',$data);

		}
	public function hapus_data($id_mapel){
		$this->db->where('id_mapel',$id_mapel);
		$this->db->delete('tb_mapel');
	}
}