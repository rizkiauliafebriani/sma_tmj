<?php

class M_home extends CI_Model{

	public function getHome ()
	{
	return $query = $this->db->get('tb_home')->result_array();
	} 

	public function getID($id_home)
	{
		return $this->db->get_where('tb_home', ['id_home' => $id_home])->row_array();
	}

	public function ubahData()
	{
		$data = [
		"judul_home" => $this->input->post('judul_home', true),
		"isi_home" => $this->input->post('isi_home', true)
		];
	
		$this->db->where('id_home', $this->input->post('id_home'));
		$this->db->update('tb_home', $data);
	}
}

