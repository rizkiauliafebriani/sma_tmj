<?php

class M_guru extends CI_Model{

	public function getGuru ()
	{
	return $query = $this->db->get('tb_guru')->result_array();
	} 

	public function input_data($data){
		$data = array(
	  	  	"nama_guru" => $this->input->post('nama_guru', true),
	        "email" => $this->input->post('email', true),
	        "nik_guru" => $this->input->post('nik_guru', true),
	        "password" =>  md5($this->input->post('password', true))
   			 );
      	return $this->db->insert('tb_guru',$data);

		}

	public function hapus_data($nik_guru){
		$this->db->where('nik_guru',$nik_guru);
		$this->db->delete('tb_guru');
	}
}