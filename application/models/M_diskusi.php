<?php

class M_diskusi extends CI_Model{

   public function getAllDiskusi ()
    {
        $this->db->select('*');
        $this->db->from('tb_diskusi');
        $this->db->join('tb_guru', 'tb_guru.nik_guru=tb_diskusi.nik_guru');
        $this->db->join('tb_kelas', 'tb_kelas.id_kelas=tb_diskusi.id_kelas');
        $this->db->order_by('tb_diskusi.id_diskusi', 'desc');
        $query = $this->db->get_where();
        return $query->result_array();
    } 

    public function getDiskusiByID($id_diskusi)
    {
        
        return $this->db->get_where('tb_diskusi', ['id_diskusi' => $id_diskusi])->row_array();
    }
}