<?php

class M_dashboard extends CI_Model{

	public function hitungJumlahMateri()
	{   
    $query = $this->db->get('tb_materi');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
    else
    {
      return 0;
    }
		}

	public function hitungJumlahTugas()
	{   
    $query = $this->db->get('tb_tugas');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
    else
    {
      return 0;
    }
		}

	public function hitungJumlahNilai()
	{   
    $query = $this->db->get('tb_nilai');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
    else
    {
      return 0;
    }
		}

	public function hitungJumlahDiskusi()
	{   
    $query = $this->db->get('tb_diskusi');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
    else
    {
      return 0;
    }
	}

  public function getInfo()
  {
  return $query = $this->db->get('tb_info')->result_array();
  } 

}
