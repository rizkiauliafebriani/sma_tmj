<div id="page-wrapper">
          <div class="header"> 
                        <h1 class="page-header">
                            Data Pengolah Diskusi
                        </h1>
                        <ol class="breadcrumb">
                      <li class="active">Tabel Data Pengolah Diskusi</li>
                    </ol> 
                                    
        </div>
<div id="page-inner">
  <?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>

            <div class="row">
                <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-responsive">
                                  <div class="card shadow mb-4">
                                      <div class="card-body">
                                          Topik     : <?php echo $varDisId['topik_diskusi']?> <font color="red"> (<?php echo $varDisId['waktu_post']?>)</font><br>
                                          Kelas     : <?php echo $varDisId['id_kelas']?><br>
                                          Oleh Guru : <?php echo $varDisId['nik_guru']?><hr>
                                                <div id="collapseTwo" class="panel-collapse in" style="height: auto;">
                                                  <div class="jumbotron">
                                                     <?php echo $varDisId['isi_diskusi']?>
                                                  </div>        
                                                </div>

                                      </div>
                                  </div>

                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
    