<div id="page-wrapper">
          <div class="header"> 
                        <h1 class="page-header">
                            Data Pengolah Diskusi
                        </h1>
                        <ol class="breadcrumb">
                      <li class="active">Tabel Data Pengolah Diskusi</li>
                    </ol> 
                                    
        </div>
<div id="page-inner">
  <?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>

            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-responsive">
                  <table class="table table-bordered table" id="dataTables-example">
                                    <thead>
                    <tr>
                      <th>Judul Diskusi</th>
                      <th>Isi Diskusi</th>
                      <th>Guru</th>
                      <th>Kelas</th>
                      <th>Waktu Post</th>                      
                      <th>Aksi</th>
                    </tr>
                  </thead>
                     <tbody>
                                        <?php  $no = 1; foreach ( $varDiskusi as $u) : ?>
                                        <tr>
                                          <td><?= $u['topik_diskusi']; ?></td>
                                          <td><?= $u['isi_diskusi']; ?></td>
                                          <td><?= $u['nik_guru']; ?></td>
                                          <td><?= $u['id_kelas']; ?></td>
                                          <td><?= $u['waktu_post']; ?></td>
                                          <td><a href="<?php echo site_url('C_diskusi/tampil/'); ?><?= $u['id_diskusi'];?>"class="btn-primary btn-sm"><span class="fi-magnifying-glass">Komentar</span></a></td>
                                        </tr>
                                     <?php endforeach; ?>
                                    </tbody>

                  </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
    