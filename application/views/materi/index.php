<div id="page-wrapper">
          <div class="header"> 
                        <h1 class="page-header">
                            Data Pengolah Materi
                        </h1>
                        <ol class="breadcrumb">
                      <li class="active">Tabel Data Pengolah Materi</li>
                    </ol> 
                                    
        </div>
<div id="page-inner">
  <?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>

            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->

                    <div class="panel panel-default">
                        <!--div class="panel-heading">
                          <a href="<?php echo base_url(); ?>C_Materi/tambah" class="btn btn-success float-right">+ Materi</a>
                        </div-->
                        <div class="panel-body">
                            <div class="table-responsive">
                  <table class="table table-bordered table" id="dataTables-example">
                                    <thead>
                    <tr>
                      <th>Kelas</th>
                      <th>Mata Pelajaran</th>
                      <th>Nama Materi</th>
                      <th>File Materi</th>                      
                      <th>Pengajar</th>
                      <th>Waktu Upload</th>
                    </tr>
                  </thead>
                   <tbody>
                                        <?php  $no = 1; foreach ( $varMateri as $u) : ?>
                                        <tr>
                                          <td><?= $u['nama_kelas']; ?></td>
                                          <td><?= $u['id_detail_mapel']; ?></td>
                                          <td><?= $u['nama_materi']; ?></td>
                                          <td><?= $u['file_materi']; ?></td>
                                          <td><?= $u['nama_guru']; ?></td>
                                          <td><?= $u['waktu_upload']; ?></td>
                                        </tr>
                                     <?php endforeach; ?>
                                    </tbody>
                   </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
    