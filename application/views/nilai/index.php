<div id="page-wrapper">
          <div class="header"> 
                        <h1 class="page-header">
                            Data Pengolah Materi
                        </h1>
                        <ol class="breadcrumb">
                      <li class="active">Tabel Data Pengolah Materi</li>
                    </ol> 
                                    
        </div>
<div id="page-inner">
  <?php if($this->session->flashdata('notif')){
  echo $this->session->flashdata('notif');
}?>

            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-responsive">
                  <table class="table table-bordered table" id="dataTables-example">
                                    <thead>
                    <tr>
                      <th>Kelas</th>
                      <th>Mata Pelajaran</th>
                      <th>Tugas</th>     
                      <th>Kategori Tugas</th>                  
                      <th>Nilai</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>IX A</td>
                      <td>Matematika</td>
                      <td>Aljabar Vektor</td>
                      <td>Tugas 1</td>
                      <td>79</td>
                    </tr>
                  </tbody>

                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
    