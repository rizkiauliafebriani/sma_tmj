<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
     <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SMA Taman Madya Jetis Yogyakarta</title>
    <!-- Bootstrap Styles-->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" />
  
    <!-- FontAwesome Styles-->
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="<?php echo base_url(); ?>assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="<?php echo base_url(); ?>assets/css/custom-styles.css" rel="stylesheet" />

    <!-- Google Fonts-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/dataTables/dataTables.bootstrap.css" >
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/Lightweight-Chart/cssCharts.css" > 
    
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https:///maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" level="navigation">
            <div class="navbar-header">

                <a class="navbar-brand" href="<?php echo base_url(); ?>index.html"><strong>SMA Taman Madya</strong></a>
            </div>

            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a aria-expanded="false">
                        <h5><font color="black"> Tanggal Sekarang : <?php echo date('d F Y'); ?></font></h5>   
                    </a>
                </li>
                <li class="nav-item">
            <a href="#" class="nav-link"><font color="white"><i>user</i> <b><?php echo $this->session->userdata('level') ?></b></a></font>
        </li>
                <li>
                    <a class="dropdown-toggle"  href="<?php echo site_url('C_login/logout'); ?>" onclick="return confirm('Yakin untuk Sign out?');">
                        <i class="fa fa-user fa-fw"></i><b>Sign Out</b>
                    </a>
                </li>
                <li>
                    <a class="dropdown-toggle"  href="<?php echo site_url('C_login/logout'); ?>" onclick="return confirm('Yakin untuk Sign out?');">
                    </a>
                </li>
            </ul>
        </nav>
        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <!--a href="<?php echo base_url('C_controller'); ?>"><i class="fa fa-qrcode"></i>Pemanggilan tanpa index.php</a-->

                    <li>
                        <a href="<?php echo site_url('C_dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('C_materi'); ?>"><i class="fa fa-desktop"></i> Materi</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('C_tugas'); ?>"><i class="fa fa-bar-chart-o"></i> Tugas</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('C_nilai'); ?>"><i class="fa fa-qrcode"></i> Nilai</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('C_diskusi'); ?>"><i class="fa fa-table"></i> Forum Diskusi</a>
                    </li>
        <?php
          if($this->session->userdata('level') == 'guru'){ // Jika level-nya admin
        ?>
                    <li>
                        <a href="<?php echo site_url('C_tugasjawaban'); ?>"><i class="fa fa-table"></i> Jawaban Tugas</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('C_tugaskategori'); ?>"><i class="fa fa-table"></i> Kategori Tugas</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('C_nilailaporan'); ?>"><i class="fa fa-table"></i> Laporan Nilai</a>
                    </li>
         <?php    }     ?>
                </ul>

            </div>

        </nav>
        </div>
     