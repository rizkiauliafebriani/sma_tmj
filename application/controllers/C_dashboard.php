<?php

class C_dashboard Extends CI_Controller{

	public function index()
	{
		if($this->M_login->logged_id())
		{

		$data['dt1']= $this->M_dashboard->hitungJumlahMateri();
		//$data['dt2']= $this->M_dashboard->hitungJumlahTugas();
		//$data['dt3']= $this->M_dashboard->hitungJumlahNilai();
		$data['dt4']= $this->M_dashboard->hitungJumlahDiskusi();
		$this->load->view('templates/sidebar');
		$this->load->view('dashboard/index', $data);
		$this->load->view('templates/footer');		

		}else{

			$this->session->set_flashdata('notif','<div class="alert alert-warning">Maaf! Silahkan Login Dahulu</div>');
			//jika session belum terdaftar, maka redirect ke halaman login
			redirect("C_login");

		}
	}


}