<?php
class C_login extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        $this->load->library(array('form_validation'));
    }


	public function index()
	{

		$this->load->view('utama/head');
		$this->load->view('login/index');
	}

	 public function proses_login(){
		if($this->M_login->logged_id())
			{
				//jika memang session sudah terdaftar, maka redirect ke halaman dahsboard
				redirect(base_url('C_dashboard')); 

			}else{

				//jika session belum terdaftar

				//set form validation
	            $this->form_validation->set_rules('username', 'Username', 'required');
	            $this->form_validation->set_rules('password', 'Password', 'required');

	            //cek validasi
				if ($this->form_validation->run() == TRUE) {

				//get data dari FORM
	            $username = $this->input->post("username", TRUE);
	            $password = MD5($this->input->post('password', TRUE));
	            
	            //checking data via model
	            $checking = $this->M_login->check_login('tb_siswa', array('username' => $username), array('password' => $password));
	            
	            //jika ditemukan, maka create session
	            if ($checking != FALSE) {
	                foreach ($checking as $apps) {

	                    $session_data = array(
	                        'siswa_id'   => $apps->id_siswa,
	                        'nama' => $apps->nama,
	                        'username' => $apps->username,
	                        'pass' => $apps->password,
	                        'level'=> $apps->level
	                        
	                    );
	                    //set session userdata
	                    $this->session->set_userdata($session_data);

	                    redirect('C_dashboard');
	                    //redirect(base_url('C_dashboard')); 
	                }
	            }else{
	            	$this->session->set_flashdata('notif','<div class="alert alert-warning">Maaf! Username / Pasword anda tidak terdaftar</div>'); // Buat session flashdata
	            	redirect(base_url('C_login')); 
	            }

	        }
		}
	}
    
            	public function logout(){
				$this->session->sess_destroy(); // Hapus semua session
				redirect('C_login');  // Redirect ke halaman login
		}
}

