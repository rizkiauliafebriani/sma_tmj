<?php

class C_diskusi Extends CI_Controller{

	public function index()
	{
        if($this->M_login->logged_id())
		{

                
       // $data['jmlKomen']= $this->M_diskusi->hitungJumlahK($id_diskusi); // menghitung jumlah komentar 
		//$dataa['varData']= $this->M_user->tampil_data(); //user login
		$data['varDiskusi'] = $this->M_diskusi->getAllDiskusi();   
		$this->load->view('templates/sidebar');
		$this->load->view('diskusi/index',$data);
		$this->load->view('templates/footer');

		}else{
			$this->session->set_flashdata('notif','<div class="alert alert-warning">Maaf! Silahkan Login Dahulu</div>');
			//jika session belum terdaftar, maka redirect ke halaman login
			redirect("C_login");

		}
	}

	function tampil($id_diskusi)
	{
		//$data['varData']= $this->M_user->tampil_data(); // pelogin <si komentar>
		$data['varDisId']= $this->M_diskusi->getDiskusiByID($id_diskusi); // si pembuat diskusi
		//$dta['komen']= $this->M_diskusi->tampil_komentar($id_diskusi); // menampilkan isi komentar
		$this->load->view('templates/sidebar');
		$this->load->view('diskusi/tampil',$data);
		$this->load->view('templates/footer');
	}
}