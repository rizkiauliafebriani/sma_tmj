<?php

class C_materi extends CI_Controller{


	public function index()
	{	
		if($this->M_login->logged_id())
		{

	    $dt['varMateri']= $this->M_materi->getAllMateri();
		$this->load->view('templates/sidebar');
		$this->load->view('materi/index', $dt);
		$this->load->view('templates/footer');

		}else{

			$this->session->set_flashdata('notif','<div class="alert alert-warning">Maaf! Silahkan Login Dahulu</div>');
			//jika session belum terdaftar, maka redirect ke halaman login
			redirect("C_login");
		}
	}
}